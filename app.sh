#!/bin/bash

INPUT=/user/dsbda/hw1/input
OUTPUT=/user/dsbda/hw1/output

srcDir="$2"
destDir="$3"

start(){
	hadoop dfsadmin -safemode leave
	echo 'remove output directory' $OUTPUT
	hadoop fs -rm -r $OUTPUT

	run="hadoop jar $srcDir/hw1-hadoop.jar $INPUT $OUTPUT"

	echo "running " $run
	eval $run
}

copy(){
	if [ -z "$srcDir" ] || [ -z "$destDir" ]; then
	    echo "Usage: $0 copy {source_dir} {dest_dir}"
	    exit 2
	fi
	arr=($srcDir/*.bz2)
	for i in "${arr[@]}"; do
	    echo "Processing $i file"
	    tarOne="bzip2 -dkf $i"
	    echo $tarOne
	    eval $tarOne
	done

	copyArr=($srcDir/*.txt)
	for i in "${copyArr[@]}"; do
	    copyOne="hadoop fs -copyFromLocal $i $destDir"
       	    echo $copyOne
	    eval $copyOne
	    rm -rf $i
	done

}

remove(){
	c="hadoop fs -rm -r -f $INPUT/* "
	echo "Remove input files from hadoop fs"
	echo $c
	eval $c
}

view(){
    files='0 1 2'
    for file in $files
    do
        f="hadoop fs -text $OUTPUT/part-r-0000$file"
        echo $f
        eval $f
    done
}

case "$1" in
	(start)
	start "$srcDir"
	exit 0;;
	(copy)
	copy "$srcDir" "$destDir"
	exit 0;;
	(remove)
	remove
	exit 0;;
	(view)
	view
	exit 0;;
	(*)
	echo "Usage: $0 {start <work directory>|copy <src> <dest>|remove} \n
	<work directory> - contains hw1-hadoop.jar"
	exit 2;;
esac