package ru.tarmazakov.mephi.dsbda.hw1.test;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.tarmazakov.mephi.dsbda.hw1.CustomCounter;
import ru.tarmazakov.mephi.dsbda.hw1.CustomPartitioner;
import ru.tarmazakov.mephi.dsbda.hw1.mapper.HighBiddingPriceMapper;
import ru.tarmazakov.mephi.dsbda.hw1.reducer.BiddingPriceReducer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test class for mapper and reducer
 */
public class MapReduceTest {
    public static final String USER_AGENT = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)";
    private static final String INPUT_STRING = "2e72d1bd7185fb76d69c852c57436d37\t20131019025500549\t1\tCAD06D3WCtf\t" + USER_AGENT + "\t113.117.187.*\t216\t234\t2\t33235ca84c5fee9254e6512a41b3ad5e\t8bbb5a81cc3d680dd0c27cf4886ddeae\tnull\t3061584349\t728\t90\tOtherView\tNa\t5\t7330\t277\t48\tnull\t2259\t10057,13800,13496,10079,10076,10075,10093,10129,10024,10006,10110,13776,10146,10120,10115,10063";
    private static final String ERROR_INPUT_STRING = "2e72d1bd7185fb76d69c852c57436d37\t20131019025500549\t1\tCAD06D3WCtf\t" + USER_AGENT + "\t113.117.187.*\t216\t2\t2\t33235ca84c5fee9254e6512a41b3ad5e\t8bbb5a81cc3d680dd0c27cf4886ddeae\tnull\t3061584349\t728\t90\tOtherView\tNa\t5\t7330\t277\t48\tnull\t2259\t10057,13800,13496,10079,10076,10075,10093,10129,10024,10006,10110,13776,10146,10120,10115,10063";

    private static final String DSBDA_HW1_CITY_EN_TXT = "/media/tarmazakov/Data/myrepo/IdeaProjects/dsbda/hw1-hadoop/src/test/resources/city.en.txt";
    public static final String ZHONGSHAN = "zhongshan";

    private MapDriver<Object, Text, Text, Text> mapDriver;
    private ReduceDriver<Text, Text, Text, IntWritable> reduceDriver;
    private MapReduceDriver<Object, Text, Text, Text, Text, IntWritable> mapReduceDriver;
    private CustomPartitioner partitioner;

    @Before
    public void setup() {
        HighBiddingPriceMapper mapper = new HighBiddingPriceMapper();
        mapper.setCitiesFilePath(DSBDA_HW1_CITY_EN_TXT);

        BiddingPriceReducer reducer = new BiddingPriceReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);

        partitioner = new CustomPartitioner();
    }


    /**
     * Testing HighBiddingPriceMapper
     */
    @Test
    public void testMapper() {
        mapDriver.withInput(new Object(), new Text(INPUT_STRING));
        mapDriver.withOutput(new Text(ZHONGSHAN), new Text(USER_AGENT + "\t" + "277"));
        mapDriver.runTest();
    }

    /**
     * Testing BiddingPriceReducer
     */
    @Test
    public void testReducer() {
        List<Text> values = new ArrayList<>();
        values.add(new Text(USER_AGENT + "\t" + "2"));
        values.add(new Text(USER_AGENT + "\t" + "297"));
        reduceDriver.withInput(new Text(ZHONGSHAN), values);
        reduceDriver.withOutput(new Text(ZHONGSHAN), new IntWritable(1));
        reduceDriver.runTest();
    }

    /**
     * Testing HighBiddingPriceMapper and BiddingPriceReducer
     */
    @Test
    public void testMapReduce() {
        mapReduceDriver.withInput(new IntWritable(), new Text(INPUT_STRING));
        mapReduceDriver.withInput(new IntWritable(), new Text(INPUT_STRING));
        mapReduceDriver.withOutput(new Text(ZHONGSHAN), new IntWritable(2));
        mapReduceDriver.runTest();
    }

    /**
     * Testing custom counter
     */
    @Test
    public void testCounter() {
        mapDriver.withInput(new Object(), new Text(ERROR_INPUT_STRING));
        mapDriver.runTest();
        assertEquals("Expected 1 counter increment", 1, mapDriver.getCounters()
                .findCounter(CustomCounter.MALFORMED_ROWS).getValue());
    }

    /**
     * Testing custom partitioner
     */
    @Test
    public void testPartitioner() {
        int partition = partitioner.getPartition(new Text(ZHONGSHAN), new Text(USER_AGENT + "\t" + "2"), 3);
        assertEquals(partition, 0);
    }
}
