package ru.tarmazakov.mephi.dsbda.hw1.test;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Utility class. Just for manual testing.
 */
public class GetDataFromLineTest {

    public static final String FILE_NAME = "test.txt";

    @Test
    public void getDataFromLine() throws IOException {

        StringBuilder result = new StringBuilder("");

        File file = new File(getClass().getClassLoader().getResource(FILE_NAME).getFile());
        try (Scanner scanner = new Scanner(file)) {
            int i = 0;
            while (scanner.hasNextLine()) {
                String s = scanner.nextLine();
                String[] line = s.split("\t");
                String userAgent = line[4];
                if (userAgent.contains("Windows")) {
                    i++;
//                    System.out.println(userAgent);
                } else if (userAgent.contains("Mac OS")) {
                    i++;
//                    System.out.println(userAgent);
                } else if (userAgent.contains("Linux")) {
                    i++;
//                    System.out.println(userAgent);
                } else {
                    System.out.println(s);
                }
                String cityId = line[7];
                String biddingPrice = line[19];
//                if (line.length != 24) Assert.fail();
            }
            System.out.println(i);


            scanner.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
