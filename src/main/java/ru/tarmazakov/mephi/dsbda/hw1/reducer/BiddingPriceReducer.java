package ru.tarmazakov.mephi.dsbda.hw1.reducer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Reducer class
 */
public class BiddingPriceReducer extends Reducer<Text, Text, Text, IntWritable> {
    /**
     * Summarizes the entries of each of the city names in the logs, where the bidding price is more than 250.
     *
     * @param key     key
     * @param values  values
     * @param context context
     * @throws IOException
     * @throws InterruptedException
     */
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        int count = 0;
        for (Text val : values) {
            String[] split = val.toString().split("\t");
            if (Integer.parseInt(split[1]) > 250) {
                count++;
            }
        }
        context.write(new Text(key), new IntWritable(count));
    }
}
