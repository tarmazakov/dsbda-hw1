package ru.tarmazakov.mephi.dsbda.hw1;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * Custom Partitioner class
 */
public class CustomPartitioner extends Partitioner<Text, Text> {

    /**
     * Split into three partitions by operation system type Windows, Mac OS, Linux
     *
     * @param key            key
     * @param value          value
     * @param numReduceTasks number of reduce tasks
     * @return partition
     */
    @Override
    public int getPartition(Text key, Text value, int numReduceTasks) {

        String[] line = value.toString().split("\t");
        String userAgent = line[0];

        if (numReduceTasks == 0) return 0;

        else if (userAgent.contains("Windows")) return 0;

        else if (userAgent.contains("Mac OS")) return 1 % numReduceTasks;

        else return 2 % numReduceTasks;
    }
}