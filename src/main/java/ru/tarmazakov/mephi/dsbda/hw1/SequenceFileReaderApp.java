package ru.tarmazakov.mephi.dsbda.hw1;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import ru.tarmazakov.mephi.dsbda.hw1.mapper.SequenceFileReaderMapper;

import java.io.IOException;

/**
 * App for reading sequence file, just fo test.
 */
public class SequenceFileReaderApp extends Configured implements Tool {

    private static final Log log = LogFactory.getLog(SequenceFileReaderApp.class);


    @Override
    public int run(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.printf("Usage: %s needs two arguments   files\n",
                    getClass().getSimpleName());
            return -1;
        }

        //Initialize the Hadoop job and set the jar as well as the name of the Job
        Job job = initJob(args[1]);

        boolean isSuccessful = job.waitForCompletion(true);
        if (isSuccessful) log.info("Job was successful");
        else log.warn("Job was not successful");

        return isSuccessful ? 0 : 1;
    }

    private Job initJob(String arg) throws IOException {
        Job job = new Job();
        job.setJarByClass(HighBidPricedByCityApp.class);
        job.setJobName("SequenceFileReader");

        FileInputFormat.addInputPath(job, new Path(arg));
        FileOutputFormat.setOutputPath(job, new Path(arg + "/txt"));

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setMapperClass(SequenceFileReaderMapper.class);

        job.setNumReduceTasks(0);
        return job;
    }
}
