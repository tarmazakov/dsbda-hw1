package ru.tarmazakov.mephi.dsbda.hw1.mapper;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Mapper class of the MapReduce package.
 * It just writes the input key-value pair to the context
 *
 * @author Raman
 */
public class SequenceFileReaderMapper extends Mapper<Text, IntWritable, Text, IntWritable> {

    /**
     * This is the map function, it does not perform much functionality.
     * It only writes key and value pair to the context
     * which will then be written into the sequence file.
     */
    @Override
    protected void map(Text key, IntWritable value, Context context) throws IOException, InterruptedException {
        context.write(key, value);
    }

}
