package ru.tarmazakov.mephi.dsbda.hw1;

/**
 * Enum with custom counter names
 */
public enum CustomCounter {
    /**
     * Counter for statistic about malformed rows
     */
    MALFORMED_ROWS
}
