package ru.tarmazakov.mephi.dsbda.hw1.mapper;

import lombok.Setter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ru.tarmazakov.mephi.dsbda.hw1.CustomCounter.MALFORMED_ROWS;

/**
 * Mapper class
 */
@Setter
public class HighBiddingPriceMapper extends Mapper<Object, Text, Text, Text> {

    private static final Log log = LogFactory.getLog(HighBiddingPriceMapper.class);
    private static final String UNDEFINED = "UNDEFINED";
    /**
     * Not constant? for test only
     */
    private String citiesFilePath;
    private Map<Integer, String> keyCityMap = new HashMap<>();


    /**
     * Setup method, prepare data for Mapper. Called once at the beginning of the task.
     *
     * @param context context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Configuration conf = context.getConfiguration();
        if (citiesFilePath == null) citiesFilePath = "/user/dsbda/hw1/city.en.txt"; // for test only
        readFile(citiesFilePath, conf);
        super.setup(context);
    }

    /**
     * Gets out the name of the city from the line, in which the bidding price is more than 250.
     * In case the city name is not found or the bidding price has an incorrect format, then the line is considered malformed.
     *
     * @param key     key
     * @param value   1 line from file
     * @param context context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        log.info(value.toString());
        String[] line = value.toString().split("\t");

        String userAgent = line[4];
        boolean isValidUserAgent = validateUserAgent(userAgent);

        String city = getCityById(line[7]);

        int biddingPrice = getBiddingPrice(line[19]);

        if (city.equals(UNDEFINED)
                || biddingPrice == -1
                || line.length != 24
                || !isValidUserAgent) {

            context.getCounter(MALFORMED_ROWS).increment(1);
        } else {
            context.write(new Text(city), new Text(userAgent + "\t" + biddingPrice));
        }
    }

    /**
     * Validate input string
     *
     * @param userAgent
     * @return
     */
    private boolean validateUserAgent(String userAgent) {
        Pattern p = Pattern.compile("(Windows|Linux|Mac OS)");
        Matcher m = p.matcher(userAgent);
        return m.find();
    }

    /**
     * Returns a Double object holding the double value represented by the argument string doubleAsString.
     *
     * @param doubleAsString the string to be parsed.
     * @return a Double object holding the value represented by the String argument.
     */
    private int getBiddingPrice(String doubleAsString) {
        try {
            return Integer.valueOf(doubleAsString);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    /**
     * Returns a String object golding the city name  represented by the id.
     * <p>
     * If city id is undefined, method return UNDEFINED.
     *
     * @param cityIdAsString city id
     * @return city name
     */
    private String getCityById(String cityIdAsString) {
        try {
            Integer cityId = Integer.valueOf(cityIdAsString);
            String city = keyCityMap.get(cityId);
            if (city == null || city.isEmpty()) return UNDEFINED;
            return city;
        } catch (NumberFormatException e) {
            log.error("city id " + cityIdAsString + " is undefined");
            return UNDEFINED;
        }
    }

    /**
     * Read file by pathString contains the key-value pairs, splitting line by \s (space) and write to local Map.
     *
     * @param pathString path to the file
     * @param conf       Configurations
     */
    private void readFile(String pathString, Configuration conf) {
        log.info("Start reading file " + pathString);
        Path path = new Path(pathString);

        try {
            FileSystem fileSystem = FileSystem.get(conf);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileSystem.open(path)));
            String line = bufferedReader.readLine();

            while (line != null) {
                String[] split = line.split("\\s+");
//                log.info("put " + split[0] + " - " + split[1] + " to map");
                keyCityMap.put(Integer.parseInt(split[0]), split[1]);
                line = bufferedReader.readLine();
            }

            bufferedReader.close();
        } catch (IOException e) {
            log.error("Error reading file " + pathString + ". " + e.getMessage());

        }
        log.info("The file " + pathString + " was read successfully");
    }
}