package ru.tarmazakov.mephi.dsbda.hw1;

import org.apache.hadoop.util.ToolRunner;

/**
 * Main class
 */
public class Main {
    /**
     * Main function.
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        ToolRunner.run(new HighBidPricedByCityApp(), args);
//        System.exit(ToolRunner.run(new HighBidPricedByCityApp(), args));
    }
}
