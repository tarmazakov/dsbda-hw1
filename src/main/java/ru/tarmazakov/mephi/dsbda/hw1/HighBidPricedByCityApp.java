package ru.tarmazakov.mephi.dsbda.hw1;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import ru.tarmazakov.mephi.dsbda.hw1.mapper.HighBiddingPriceMapper;
import ru.tarmazakov.mephi.dsbda.hw1.reducer.BiddingPriceReducer;

import java.io.IOException;

import static ru.tarmazakov.mephi.dsbda.hw1.CustomCounter.MALFORMED_ROWS;

/**
 *  HighBidPricedByCity Application
 */
public class HighBidPricedByCityApp extends Configured implements Tool {
    private static final Log log = LogFactory.getLog(HighBidPricedByCityApp.class);

    /**
     *
     * Create and run hadoop job
     *
     * @param args arguments
     * @return job status
     * @throws Exception
     */
    @Override
    public int run(String[] args) throws Exception {
        this.setConf(new Configuration());

        if (args.length < 2) {
            log.error("Error input format. Please use:\n" +
                    " hadoop jar <jar_name>.jar <path_to_input_files> <path_to_output_files>");
            System.exit(1);
        }

        log.info("Starting application");

        Job job = initJob(args);

        boolean isSuccessful = job.waitForCompletion(true);
        if (isSuccessful) log.info("Job was successful");
        else log.warn("Job was not successful");

        Counter counter = job.getCounters().findCounter(MALFORMED_ROWS);
        log.info("Malformed rows count:" + counter.getValue());

        return isSuccessful ? 0 : 1;
    }

    /**
     * Initialize and return Job instance
     *
     * @param args args with input and output paths
     * @return Job
     * @throws IOException
     */
    private Job initJob(String[] args) throws IOException {
        Job job = Job.getInstance(this.getConf(), "Amount of high-bid-prices by city");
        job.setJarByClass(HighBidPricedByCityApp.class);


        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setMapperClass(HighBiddingPriceMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        job.setReducerClass(BiddingPriceReducer.class);
        job.setNumReduceTasks(3);
        job.setInputFormatClass(TextInputFormat.class);

        job.setPartitionerClass(CustomPartitioner.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        return job;
    }

}
