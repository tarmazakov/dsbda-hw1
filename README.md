This hadoop MapReduce Application that implements the following logic:

For dataset http://data.computational-advertising.org calculate amount of high-bid- priced
(more than 250) impression events by city, where each city presented with its name rather
than id. Additional info about contest: http://contest.ipinyou.com/ipinyou-dataset.pdf. (Use
only imp.201310(19-27).txt.bz2 files as a main dataset)

